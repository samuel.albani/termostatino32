### Termostatino32

*Autore*: Samuel Albani

*Descrizione*: Il progetto consiste in un termostato realizzato con il microcontrollore ESP32.
Viene fatto uso di un termistore NTC per monitorare la temperatura (calcolata con la formula riportata su https://it.wikipedia.org/wiki/Termistore#Equazione_con_parametro_B),
ed un potenziometro per regolare la temperatura desiderata tra 20 e 40 °C;
l'innalzamento della temperatura oltre la somma tra quella desiderata ed una soglia di 2 °C (per evitare continue accensioni e spegnimenti
dovuti a disturbi di lettura) provoca l'accensione della ventola,
la cui velocità è controllata in PWM attraverso un transistor, in base alla differenza tra la temperatura rilevata e quella desiderata;
quando la temperatura rilevata scende sotto quella desiderata, la ventola si ferma.
Lo stato del sistema è indicato, oltre che attraverso dati inviati su seriale, anche da un LED RGB regolato in PWM che si accende di blu
se la temperatura è sufficientemente bassa, mentre sfuma dal verde al rosso man mano che la temperatura rilevata sale oltre quella desiderata,
proporzionalmente alla velocità della ventola.
Inoltre, vengono inviati dati su temperatura attuale, temperatura desiderata e velocità della ventola percentuale ad un broker MQTT sotto il topic "Termostatino32",
attraverso la libreria PubSubClient, ed è possibile impostare la temperatura desiderata anche via MQTT tramite "Termostatino32/imposta <valore_double>" (sottinteso in °C);
per tornare invece al controllo manuale della temperatura desiderata tramite potenziometro basta inviare via MQTT "Termostatino32/imposta " (con payload vuota).

*Repository*: https://gitlab.com/samuel.albani/termostatino32

*Licenza scelta*: GNU GPLv3

*Data di presentazione*: 16 luglio 2019
